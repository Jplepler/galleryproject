#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <string>
#include "MemoryAccess.h"
#include "AlbumManager.h"
#include "DatabaseAccess.h"

using namespace std;

int getCommandNumberFromUser()
{
	string message("\nPlease enter any command(use number): ");
	string numericStr("0123456789");
	
	cout << message << endl;
	string input;
	getline(cin, input);
	
	while (cin.fail() || cin.eof() || input.find_first_not_of(numericStr) != string::npos) {

		cout << "Please enter a number only!" << endl;

		if (input.find_first_not_of(numericStr) == string::npos) {
			cin.clear();
		}

		cout << endl << message << endl;
		getline(cin, input);
	}
	
	return atoi(input.c_str());
}

/*
This function prints the author and activation details
*/
void PrintSystemInfo()
{				
	time_t now = time(0);
	char* dt = ctime(&now);//Get the date, time and year
	cout << "Author - Jonathan Plepler" << endl
		 << "Activation date and time - " << dt << endl;

}


int main(void)
{
	// initialization data access
	DatabaseAccess dataAccess;

	// initialize album manager
	AlbumManager albumManager(dataAccess);


	std::string albumName;
	PrintSystemInfo();

	std::cout << "Welcome to Gallery!" << std::endl;
	std::cout << "===================" << std::endl;
	std::cout << "Type " << HELP << " to a list of all supported commands" << std::endl;

	do {
		int commandNumber = getCommandNumberFromUser();
		
		try	{
			albumManager.executeCommand(static_cast<CommandType>(commandNumber));
		} catch (exception& e) {	
			cout << e.what() << endl;
		}
	} 
	while (true);
}


