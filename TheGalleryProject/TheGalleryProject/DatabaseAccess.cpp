#include "DatabaseAccess.h"

using namespace std;


vector<string> DatabaseAccess::table;



int DatabaseAccess::callback(void* data, int argc, char** argv, char** azColName)
{
	for (size_t i = 0; i < argc; i++)
	{
		table.push_back(argv[i] == nullptr ? "" : argv[i]);
	}
	
	return 0;
}




/*C'Tor
_db gets an value in the open function
*/
DatabaseAccess::DatabaseAccess()
{
	_db = nullptr;
}

/*HELPER FUNCTION		IMPORTANT NOTE: This function doesnt support Queries that ask for data e.g SELECT
This function Send a query to the database, 
In: The query
Out: True if query succesded, false otherwise
*/
bool DatabaseAccess::SendQuery(const char * query)
{
	char* errMessage = nullptr;
	char sqlStatement[200] = "";
	strcpy(sqlStatement, query);
	int res = sqlite3_exec(_db, sqlStatement, nullptr, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		cerr << (errMessage) << endl;;
		return false;
		
	}
	return true;
}



/*
This functions opens the data base if exists and creates a new one otherwise
Out: true if succeded, false otherwise
*/
bool DatabaseAccess::open()
{
	
	string dbFileName = FILE_NAME;
	int doesFileExist = _access(dbFileName.c_str(), 0);
	int res = sqlite3_open(dbFileName.c_str(), &_db);

	if (res != SQLITE_OK) 
	{
		_db = nullptr;
		cout << "Failed to open DB" << endl;
		return false;
	}

	if (doesFileExist != SQLITE_OK)//if file wasn't found (0 = found)
	{
		//Try to create All neccessery tables
		if ((SendQuery("CREATE TABLE USERS (ID INTEGER PRIMARY KEY, NAME TEXT NOT NULL);") &&
			SendQuery("CREATE TABLE ALBUMS (ID INTEGER PRIMARY KEY, NAME TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, USER_ID INTEGER, FOREIGN KEY(USER_ID) REFERENCES USERS(ID));") &&
			SendQuery("CREATE TABLE PICTURES (ID INTEGER PRIMARY KEY, NAME TEXT NOT NULL, LOCATION TEXT NOT NULL, CREATION_DATE TEXT NOT NULL, ALBUM_ID INTEGER, FOREIGN KEY(ALBUM_ID) REFERENCES ALBUMS(ID));") &&
			SendQuery("CREATE TABLE TAGS (ID INTEGER PRIMARY KEY, PICTURE_ID INTEGER, USER_ID INTEGER, FOREIGN KEY(PICTURE_ID) REFERENCES PICTURES(ID), FOREIGN KEY(USER_ID) REFERENCES USERS(ID));")))
		{

		}
		else
		{
			close();//Close before ending
			return false;
		}

	}


	return true;
}



/*
This function Closes the data base connection
*/
void DatabaseAccess::close()
{
	sqlite3_close(_db);
	_db = nullptr;
}



/*
Functions sends 4 queries for creating all tables in data base without removing them completley.
*/
void DatabaseAccess::clear()
{
	SendQuery("DELETE USERS;");
	SendQuery("DELETE ALBUMS;");
	SendQuery("DELETE PICTURES;");
	SendQuery("DELETE TAGS;");
}



/*
This functions sends a query to get all albums
*/
const std::list<Album> DatabaseAccess::getAlbums()
{
	table.clear();
	list<Album> listOfAlbums;
	Album album;
	char* errMessage = nullptr;
	string sqlStatement = "SELECT NAME, CREATION_DATE, USER_ID FROM ALBUMS;";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}


	
	for (size_t i = 0; i < table.size(); i=i+3)
	{
		album.setName(table[i]);
		album.setCreationDate(table[i+1]);
		album.setOwner(stoi(table[i+2]));
		listOfAlbums.push_back(album);
		
	}
	
		
	

	return listOfAlbums;
}



const std::list<Album> DatabaseAccess::getAlbumsOfUser(const User& user)
{
	table.clear();
	list<Album> listOfAlbums;
	Album album;
	char* errMessage = nullptr;
	string sqlStatement = "SELECT * FROM ALBUMS WHERE USER_ID = " + to_string(user.getId()) + ";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	for (size_t i = 0; i < table.size(); i=i+3)
	{
		album.setName(table[i]);
		album.setCreationDate(table[i+1]);
		album.setOwner(stoi(table[i+2]));
		listOfAlbums.push_back(album);
	}


	return listOfAlbums;

}



void DatabaseAccess::createAlbum(const Album& album)
{
	string query = "INSERT INTO ALBUMS(NAME, CREATION_DATE, USER_ID) VALUES('" + album.getName() + "', '" + album.getCreationDate() + "', '" + to_string(album.getOwnerId()) + "');";
	
	if (!SendQuery(query.c_str()))
	{
		throw MyException("Cant Create Album " + album.getName());
	}

}



void DatabaseAccess::deleteAlbum(const std::string& albumName, int userId)
{
	string query1 = "DELETE FROM TAGS WHERE TAGS.PICTURE_ID IN (SELECT PICTURES.ID FROM PICTURES INNER JOIN ALBUMS ON PICTURES.ALBUM_ID=ALBUMS.ID WHERE ALBUMS.NAME = '" + albumName + "' AND ALBUMS.USER_ID = " + to_string(userId) + ");";
	string query2 = "DELETE FROM PICTURES WHERE PICTURES.ALBUM_ID IN (SELECT ALBUMS.ID FROM ALBUMS WHERE ALBUMS.NAME = '" + albumName + "' AND ALBUMS.USER_ID = " + to_string(userId) + ");";
	string query3 = "DELETE FROM ALBUMS WHERE ALBUMS.NAME = '" + albumName + "' AND ALBUMS.USER_ID = " + to_string(userId) + ";";
	
	
	if (!(SendQuery(query1.c_str()) &&	//Delete the related tags 
		  SendQuery(query2.c_str()) &&	//Delete the related pictures
		  SendQuery(query3.c_str())))	//Delete the Album
	{
		throw MyException("Cant delete album " + albumName);
	}

	
}



bool DatabaseAccess::doesAlbumExists(const std::string& albumName, int userId)
{
	table.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT * FROM ALBUMS WHERE NAME = '" + albumName + "';";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	return (!table.empty());
}



Album DatabaseAccess::openAlbum(const std::string& albumName)
{
	table.clear();
	list<Album> listOfAlbums;
	Picture picture(1, "1");
	char* errMessage = nullptr;
	string sqlStatement = "SELECT ALBUMS.NAME AS ALBUM_NAME, ALBUMS.CREATION_DATE AS ALBUM_DATE, ALBUMS.USER_ID AS USER_ID, PICTURES.NAME AS PICTURE_NAME, PICTURES.ID AS PICTURE_ID, PICTURES.LOCATION AS PICTURE_LOCATION, PICTURES.CREATION_DATE AS PICTURE_DATE, TAGS.USER_ID AS TAGGED_ID, TAGS.PICTURE_ID AS TAGGED_PICTURE FROM ALBUMS LEFT JOIN PICTURES ON ALBUMS.ID=PICTURES.ALBUM_ID LEFT JOIN TAGS ON PICTURES.ID=TAGS.PICTURE_ID WHERE ALBUMS.NAME = '" + albumName + "';";
	Album* temp = nullptr;
	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	for (size_t i = 0; i < table.size(); i+=9)
	{
		openedAlbum.setName(table[i]);
		openedAlbum.setCreationDate(table[i+1]);
		openedAlbum.setOwner(stoi(table[i+2]));
		
		//picture
		picture.setName(table[i+3]);
		
		if (picture.getName() != "")
		{

			if (!openedAlbum.doesPictureExists(picture.getName()))
			{
				picture.setId(stoi(table[i + 4]));
				picture.setPath(table[i + 5]);
				picture.setCreationDate(table[i + 6]);
				openedAlbum.addPicture(picture);//This function wont add the picture if it exists already
			}

			//tag
			if (table[i+7] != "")
			{
				openedAlbum.getPictures().back().tagUser(User(stoi(table[i + 7]), table[i + 8]));
			}
			
		}
		
	} 


	return openedAlbum;
}



void DatabaseAccess::closeAlbum(Album& pAlbum)
{

}



/*
This function send a query to get all albums
And then prints them using callbackPrint
*/
void DatabaseAccess::printAlbums()
{
	table.clear();
	
	char* errMessage = nullptr;
	string sqlStatement = "SELECT * FROM ALBUMS;";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException("Cant Print Albums");
	}
	for (size_t i = 0; i < table.size(); i+=4)
	{
		cout << "ID= " << table[i] << ", Album name= " << table[i+1] << ", Creation date= " << table[i+2] << ", @" << table[i+3] << endl;
	}
}



/*
This function Add a picture to the PICTURES table
In: name of album in which the picture will be added, the picture
*/
void DatabaseAccess::addPictureToAlbumByName(const std::string& albumName, const Picture& picture)
{
	string query = "INSERT INTO PICTURES(ID, NAME, LOCATION, CREATION_DATE, ALBUM_ID) SELECT " + to_string(picture.getId()) + ", '" + picture.getName() + "', '" + picture.getPath() + "', '" + picture.getCreationDate() + "', ID FROM ALBUMS WHERE NAME = '" + albumName + "';";
	if (!SendQuery(query.c_str()))
	{
		throw MyException("Cant Add picture to " + albumName);
	}
}



/*
This function removes a picture from the PICTURES table and delete all the tags in the picture
In: name of album in which the picture will be added, the picture
*/
void DatabaseAccess::removePictureFromAlbumByName(const std::string& albumName, const std::string& pictureName)
{
	string query1 = "DELETE FROM TAGS WHERE TAGS.PICTURE_ID IN (SELECT PICTURES.ID FROM PICTURES INNER JOIN ALBUMS ON PICTURES.ALBUM_ID=ALBUMS.ID WHERE ALBUMS.NAME = '" + albumName + " AND PICTURES.NAME = " + pictureName + "');";
	string query2 = "DELETE FROM PICTURES WHERE PICTURES.ALBUM_ID IN (SELECT ALBUMS.ID FROM ALBUMS WHERE ALBUMS.NAME = '" + albumName + " AND PICTURES.NAME = " + pictureName + "');";

	if(!(SendQuery(query1.c_str()) && SendQuery(query2.c_str())))
	{
		throw MyException("Cant remove picture from " + albumName);
	}
}



/*
This function adds a tag to the TAGS table
In: album and picture in which the a user will be tagged, the id of the user
*/
void DatabaseAccess::tagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	string query = "INSERT INTO TAGS(PICTURE_ID, USER_ID) SELECT ID," + to_string(userId) + " FROM PICTURES WHERE NAME = '" + pictureName + "';";
	if (!SendQuery(query.c_str()))
	{
		throw MyException("Cant tag user " + to_string(userId) + " to " + pictureName);
	}
}



/*
Remove a tag from TAGS table
*/
void DatabaseAccess::untagUserInPicture(const std::string& albumName, const std::string& pictureName, int userId)
{
	string query = "DELETE FROM TAGS FROM TAGS INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID INNER JOIN ALBUMS ON ALBUMS.ID = PICTURES.ALBUM_ID WHERE ALBUMS.NAME = " + albumName + " AND PICTURE.NAME = " + pictureName + "AND TAGS.USER_ID = " + to_string(userId) +";";
	if (!SendQuery(query.c_str()))
	{
		throw MyException("Cant untag user " + to_string(userId) + " from " + pictureName);
	}
}



//Send a query to get all the users, then 'callbackPrint' prints them
void DatabaseAccess::printUsers() 
{
	table.clear();
	
	char* errMessage = nullptr;
	string sqlStatement = "SELECT NAME, ID FROM USERS;";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	for (size_t i = 0; i < table.size(); i+=2)
	{
		cout << table[i] << '@' << table[i+1] << endl;
	}
}



//This function sends a query to get a user by his ID
User DatabaseAccess::getUser(int userId)
{
	size_t i = 0;
	table.clear();
	User user(1, "1");
	void* avg = 0;
	char* errMessage = nullptr;
	string sqlStatement = "SELECT * FROM USERS WHERE ID = " + to_string(userId) + ";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	
	

	return User(stoi(table[i]),table[i+1]);
}



//This function add a user to the USERS table
void DatabaseAccess::createUser(User& user)
{
	string query = "INSERT INTO USERS(ID, NAME) VALUES(" + to_string(user.getId()) + ", '" + user.getName() +"');";
	if (!SendQuery(query.c_str()))
	{
		throw MyException("Cant create user");
	}
}



/*
This function send 3 queries to delete a user
1 - Delete user from USERS table
2 - delete his albums
3 - delete his tags
4 - delete his pictures
*/
void DatabaseAccess::deleteUser(const User& user)
{
	string query1 = "DELETE FROM USERS WHERE ID = "+ to_string(user.getId()) + ";";
	string query2 = "DELETE FROM PICTURES INNER JOIN PICTURES ON PICTURES.ALBUM_ID = ALBUMS.ID WHERE ALBUMS.USER_ID = " + to_string(user.getId()) + ";";
	string query3 = "DELETE FROM ALBUMS WHERE USER_ID = " + to_string(user.getId()) + ";";
	string query4 = "DELETE FROM TAGS WHERE USER_ID = " + to_string(user.getId()) + ";";
	
	if (!(SendQuery(query1.c_str()) && SendQuery(query2.c_str()) && SendQuery(query3.c_str()) && SendQuery(query3.c_str())))
	{
		throw MyException("Cant delete user");
	}
}



/*
This function checks if a user exists
*/
bool DatabaseAccess::doesUserExists(int userId)
{
	table.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT * FROM USERS WHERE ID = " + to_string(userId) + ";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	

	return !table.empty();
}



/*
This function counts how much albums belonge to the user
*/
int DatabaseAccess::countAlbumsOwnedOfUser(const User& user)
{
	table.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT COUNT() FROM ALBUMS WHERE USER_ID = " + to_string(user.getId()) + ";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	return stoi(*(table.begin()));
}



/*
This function counts in how much album the user was tagged
*/
int DatabaseAccess::countAlbumsTaggedOfUser(const User& user)
{
	table.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT COUNT(*) FROM TAGS INNER JOIN PICTURES ON PICTURES.ID = TAGS.PICTURE_ID WHERE TAGS.USER_ID = " + to_string(user.getId()) + ";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	return stoi(*(table.begin()));
}



/*
This function counts how many times the user was tagged
*/
int DatabaseAccess::countTagsOfUser(const User& user)
{
	table.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT COUNT(*) FROM TAGS WHERE USER_ID = " + to_string(user.getId()) + ";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	return stoi(*(table.begin()));
}



/*
This function calclutes average tags per album (sum of all tags / number of albums)
*/
float DatabaseAccess::averageTagsPerAlbumOfUser(const User& user)
{
	table.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT AVG(T.CNT) number_TAGS FROM(SELECT ALBUM_ID, COUNT(*) AS CNT FROM TAGS INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID WHERE TAGS.USER_ID = " + to_string(user.getId()) + " GROUP BY ALBUM_ID) T";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);
	
	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	return (table[0] != "" ? stof(table[0]) : 0 );
}



/*
This function sends a query to find the most tagged user
*/
User DatabaseAccess::getTopTaggedUser()
{
	table.clear();
	size_t i = 0;
	void* avg = 0;
	char* errMessage = nullptr;
	string sqlStatement = "SELECT TAGS.USER_ID, USERS.NAME FROM TAGS INNER JOIN USERS ON TAGS.USER_ID=USERS.ID GROUP BY USER_ID HAVING COUNT(*) = (SELECT MAX(CNT) FROM(SELECT COUNT(*) AS CNT FROM TAGS GROUP BY USER_ID)TMP);";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	return User(stoi(table[i]), table[i+1]);
}



/*
This function finds the picture with most tags
*/
Picture DatabaseAccess::getTopTaggedPicture()
{
	size_t i = 0;
	table.clear();
	
	void* avg = 0;
	char* errMessage = nullptr;
	string sqlStatement = "SELECT TAGS.PICTURE_ID, PICTURES.NAME, PICTURES.LOCATION, PICTURES.CREATION_DATE FROM TAGS INNER JOIN PICTURES ON TAGS.PICTURE_ID = PICTURES.ID GROUP BY USER_ID HAVING COUNT(*) = (SELECT MAX(CNT) FROM(SELECT COUNT(*) AS CNT FROM TAGS GROUP BY USER_ID)TMP);";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	return Picture(stoi(table[i]), table[i+1], table[i+2], table[i+3]);
}



/*
This function sends a quarry to find all tagged pictures of the user
*/
std::list<Picture> DatabaseAccess::getTaggedPicturesOfUser(const User& user)
{
	table.clear();
	list<Picture> picturesList;
	char* errMessage = nullptr;
	string sqlStatement = "SELECT * FROM PICTURES INNER JOIN TAGS ON TAGS.PICTURE_ID = PICTURES.ID WHERE TAGS.USER_ID = " + to_string(user.getId()) + ";";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	for (size_t i = 0; i < table.size(); i+=4)
	{
		picturesList.push_back(Picture(stoi(table[i]), table[i+1], table[i+2], table[i+3]));
	}

	return picturesList;
}




int DatabaseAccess::findNextUserId()
{
	table.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT * FROM USERS WHERE ID = (SELECT MAX(ID) FROM USERS);";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}
	return (table.empty() ? 0 : stoi(table[0]));
}



int DatabaseAccess::findNextPictureId()
{
	table.clear();
	char* errMessage = nullptr;
	string sqlStatement = "SELECT * FROM PICTURES WHERE ID = (SELECT MAX(ID) FROM PICTURES);";

	int res = sqlite3_exec(_db, sqlStatement.c_str(), callback, nullptr, &errMessage);

	if (res != SQLITE_OK)
	{
		throw MyException(errMessage);
	}

	return (table.empty() ? 0 : stoi(table[0]));
}